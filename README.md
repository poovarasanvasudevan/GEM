# GEM Player
###A Fully Open Source Music Player for Android.
GEM is a part of the Substance Open Source suite of apps & promises to be nimble, functional, and customizable.

You can learn more about our project [here](http://substanceproject.net).
Feel free to report any issues you may find, or submit patches or features.

And of course, as with all of our apps, Gem is licenced under [GPL v3](http://www.gnu.org/licenses/gpl-3.0.html) :yum:.


![GEM's album view](http://substanceproject.net/images/gem/gem_album_view_small.png)

